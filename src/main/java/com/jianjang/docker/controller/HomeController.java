package com.jianjang.docker.controller;

import com.alibaba.fastjson.JSONObject;
import com.jianjang.docker.utils.InetUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description: 主页
 * @author: JianJang
 * @create: 2022-04-15 17:01
 * @blame BOSS Team
 */
@RestController
@RequestMapping("/")
public class HomeController {
    @GetMapping("index.do")
    public JSONObject index(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code","000000");
        jsonObject.put("message","交易成功");
        return jsonObject;
    }

    @GetMapping("hello")
    public JSONObject hello() throws Exception{
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code","000000");
        jsonObject.put("message","交易成功");
        JSONObject data = new JSONObject();
        data.put("localIp", InetUtils.getLocalIP());
        data.put("hostname", InetUtils.getLocalHostName());
        jsonObject.put("data",data);
        return jsonObject;
    }
}